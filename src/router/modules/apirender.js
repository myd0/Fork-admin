import Layout from '@/layout'
// import KeepAliveLayout from '@/layout/keepAlive'

export default {
    path: '/apirender',
    component: Layout,
    redirect: '/apirender/',
    name: 'database',
    meta: {
        title: '接口操作',
        icon: 'breadcrumb'
    },
    children: [
        {
            path: 'create',
            name: 'apirenderCreate',
            component: () => import(/* webpackChunkName: 'breadcrumb_example' */ '@/views/apirender/list'),
            meta: {
                title: 'API生成'
            }
        },
        {
            path: 'edit',
            name: 'apirenderEdit',
            component: () => import(/* webpackChunkName: 'breadcrumb_example' */ '@/views/apirender/edit'),
            meta: {
                title: '编辑',
                sidebar: false
            }
        }
    ]
}
