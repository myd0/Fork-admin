import {
    api
} from '@/api'

const state = {
    list: []
}

const getters = {}

const actions = {
    getApicode({state}, data) {
        return new Promise((reslove, reject) => {
            api.post('generate/create', data).then(res => {
                reslove(res)
            }).catch(() => {
                reject(state)
            })
        })
    }
}

const mutations = {
}

export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations
}
