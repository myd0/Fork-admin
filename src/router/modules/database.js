import Layout from '@/layout'
// import KeepAliveLayout from '@/layout/keepAlive'

export default {
    path: '/database',
    component: Layout,
    redirect: '/database/connect',
    name: 'database',
    meta: {
        title: '数据库操作',
        icon: 'breadcrumb'
    },
    children: [
        {
            path: 'connect',
            name: 'DatabaseConnect',
            component: () => import(/* webpackChunkName: 'breadcrumb_example' */ '@/views/database/connect'),
            meta: {
                title: '数据库连接'
            }
        },
        {
            path: 'tables',
            name: 'DatabaseTables',
            component: () => import(/* webpackChunkName: 'breadcrumb_example' */ '@/views/database/tables'),
            meta: {
                title: '数据表结构'
            }
        },
        {
            path: 'tablesList',
            name: 'DatabaseTablelist',
            component: () => import(/* webpackChunkName: 'breadcrumb_example' */ '@/views/database/tablelist'),
            meta: {
                title: '表数据'
            }
        },
        {
            path: 'tablesCreate',
            name: 'DatabaseCreate',
            component: () => import(/* webpackChunkName: 'breadcrumb_example' */ '@/views/database/create'),
            meta: {
                title: '编辑',
                sidebar: false
            }
        }
    ]
}
